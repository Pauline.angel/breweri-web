import { BeerType } from './beerType.model';

export class Beer {
    public beerType: BeerType;
    public beerTypeName: string;
    public temp: number;
    public beerId: number;
    public msg: string;
}