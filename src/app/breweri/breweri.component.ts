import { Component, OnInit } from '@angular/core';
import { BeerContainer } from './beerContainer.model';
import { Beer } from './beer.model';
import { BeerType } from './beerType.model';
import { BreweriService } from './breweri.service';
import { isArray } from 'util';




@Component({
    selector: 'app-breweri',
    templateUrl: './breweri.component.html',
    styleUrls: ['./breweri.component.css'],
    providers: [BreweriService]
})
export class BreweriComponent implements OnInit {
    //list control
    containerList: BeerContainer[];

    constructor(private breweriService: BreweriService){}

    ngOnInit() {
        this.breweriService.getAllContainers().subscribe((
            (response) => this.containerList = response
        ));
    }

    //fake controllers
    containerIdController: number = 0;
    beerIdController: number = 0;

    //selection controllers
    selectedContainer: number;
    addBeerToContainerVisible: boolean = true;
    newBeerTypeSelected: string = "-1";

    addContainer() {
        this.breweriService.createContainer().subscribe((
            (response) => this.containerList.push(response)
        ));
    }

    removeContainer() {

    }

    openContainerDoor(containerId:number) {
        console.log("Opening the container " + containerId + " door");
        this.breweriService.openContainerDoor(containerId).subscribe((
            (response) => this.updateContainer(response)
        ));
    }

    closeContainerDoor(containerId:number) {
        this.breweriService.closeContainerDoor(containerId).subscribe((
            (response) => this.updateContainer(response)
        ));
    }

    showAddBeerToContainerForm(containerId: number) {
        this.selectedContainer = containerId;
        this.addBeerToContainerVisible = false;
    }
    
    hideAddBeerToContainerForm() {
        this.addBeerToContainerVisible = true;
    }

    addBeerToContainer() {
        console.log("Adding beer to this container:" + this.selectedContainer);
        var beer = new Beer();
        beer.beerId = this.beerIdController++;
        beer.beerType = new BeerType();
        console.log(this.newBeerTypeSelected);
        //beer.beerType.beerTypeName = this.newBeerTypeSelected;
        beer.beerTypeName = this.newBeerTypeSelected;

        if (!isArray(this.containerList[this.selectedContainer].beerList)) {
            this.containerList[this.selectedContainer].beerList = [];
        }
        this.containerList[this.selectedContainer].doorOpened = true;
        this.breweriService.addBeerToContainer(this.selectedContainer, beer).subscribe((
            (response) => this.containerList[this.selectedContainer].beerList.push(response)
        ));
    }

    getBeerFromContainer(containerId: number, beerId: number) {
        this.containerList[this.getContainerIdx(containerId)].doorOpened = true;
        this.breweriService.getBeerFromContainer(containerId, beerId).subscribe((
            (response) => this.removeBeerFromListContainer(containerId, response)
        ));
    }

    private getContainerIdx(containerId: number) : number {
        return this.containerList.map(cont => cont.containerId).indexOf(containerId);
    }

    private getBeerIdx(containerId: number, beerId: number) : number {
        const container = this.containerList[this.getContainerIdx(containerId)];
        return container.beerList.map(b => b.beerId).indexOf(beerId);
    }

    private updateContainer(updatedContainer: BeerContainer) {
        const ucIdx = this.getContainerIdx(updatedContainer.containerId);
        this.containerList[ucIdx] = updatedContainer;
    }

    private removeBeerFromListContainer(containerId: number, updatedBeer: Beer) {
        var bl = this.containerList[this.getContainerIdx(containerId)].beerList;
        bl.slice(this.getBeerIdx(containerId, updatedBeer.beerId), 1);
    }
    

}