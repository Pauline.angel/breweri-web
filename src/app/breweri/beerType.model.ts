export class BeerType {
    public beerTypeName: string;
    public maxTemp: number;
    public minTemp: number;
}